/**
 * Get char codes for a given string
 * @param string
 * @returns {number[]}
 */
function string2CharCodes(string) {
    return string.split('')
        .map(function (char) {
            return char.charCodeAt(0);
        });
}

function countValuesInArray(vector) {
    let count = {};

    vector.forEach(function (i) {
        count[i] = (count[i] || 0) + 1;
    });

    return count;
}

function sortObjectByKey(unorderedObject) {
    const orderedObject = {};

    Object.keys(unorderedObject)
        .sort()
        .forEach(function (key) {
            orderedObject[key] = unorderedObject[key];
        });

    return orderedObject;
}

function parseAttributesForFirstObject(mainObject, objectToParse) {
    let finalObject = {};

    //parsing attributes from secondary object
    Object.keys(objectToParse)
        .map(function (key) {
            finalObject[key] = mainObject[key] || 0;
        });

    //parsing attributes from main object
    Object.keys(mainObject)
        .map(function (key) {
            finalObject[key] = mainObject[key] || 0;
        });

    return finalObject;
}

function charFrequencies(sentence1) {
    return countValuesInArray(string2CharCodes(sentence1.toLowerCase())
        .sort(function sortAscending(a, b) {
            return a - b;
        }));
}

function termFrequencies(sentence) {
    let termCollection = {};

    sentence.toLowerCase().split(/\s+/g)
        .map(function (word) {
            if (!termCollection[word]) {
                termCollection[word] = 1;
            }
            else {
                termCollection[word]++;
            }
        });

    return sortObjectByKey(termCollection);
}

function getCosine(A, B) {
    // COS(theta) = SUM(A*B)/( SQUARE(SUM(A^2))*SQUARE(SUM(B^2)) )

    //TODO REFACTOR THIS
    //AB = SUM(A*B)
    let AB = A.map(function (el, index) {
        return el * B[index];
    })
        .reduce(function (pv, cv) {
            return pv + cv;
        });

    //SA2 = (SUM(A^2))
    let SA2 = A.map(function (el) {
        return el * el;
    })
        .reduce(function (pv, cv) {
            return pv + cv;
        });

    //B2 = (SUM(B^2)
    let SB2 = B.map(function (el) {
        return el * el;
    })
        .reduce(function (pv, cv) {
            return pv + cv;
        });

    return AB / (Math.sqrt(SA2) * Math.sqrt(SB2));
}

/**
 * Measures similarity between two Strings calculating the cosine of the angle between them.
 * @param {string} string1
 * @param {string} string2
 * @param {boolean} termFrequency - Considers Terms instead Chars if set. False by default.
 * @return {*} - Returns a value between 0 and 1. The cosine of the angle between inputs.
 */
function cosSimilarity(string1, string2, termFrequency = false) {
    let v1;
    let v2;

    if (!termFrequency) {
        // removing empty spaces
        string1 = string1.replace(/\s+/g, '');
        string2 = string2.replace(/\s+/g, '');
        //get char frequencies
        v1 = charFrequencies(string1);
        v2 = charFrequencies(string2);
    }
    else {
        // removing special chars
        string1 = string1.replace(/[^a-zA-Z0-9]/gmi, ' ');
        string2 = string2.replace(/[^a-zA-Z0-9]/gmi, ' ');
        //get term frequencies
        v1 = termFrequencies(string1);
        v2 = termFrequencies(string2);
    }

    const A = Object.values(sortObjectByKey(parseAttributesForFirstObject(v1, v2)));
    const B = Object.values(sortObjectByKey(parseAttributesForFirstObject(v2, v1)));

    return getCosine(A, B);
}


module.exports = cosSimilarity;