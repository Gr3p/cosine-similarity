# Cos1ne Similarity

Measures similarity between two Strings calculating the cosine of the angle between them.

Returns a value **between 0** *(completely different)* **and 1** *(same string)*. 

| Param  | Type                | Description  |
| ------ | ------------------- | ------------ |
|string1 | <code>string</code> |  first string to compare |
|string2| <code>string</code> |  second string to compare  |
|termFrequency| <code>boolean</code> |  Considers Terms instead Chars if is set. False by default.  |


## Install

```bash
npm i cos1ne-similarity
```


## Examples


```js
const cosSimilarity= require('cos1ne-similarity'); 

const text= 'I want eat';

const pool = [
    'Drink me!',
    'Eat me!',
    'I would like something to eat'
];

console.log('#Comparing:', text);
for (let i = 0; i < pool.length; i++) {
    const similarity = cosSimilarity(text, pool[i]);
    console.log('#TEXT:', pool[i], '#->', similarity);
}

// #Comparing: I want eat
// #TEXT: Drink me! #-> 0.30618621784789724
// #TEXT: Eat me! #-> 0.6123724356957945
// #TEXT: I would like something to eat #-> 0.6531972647421809

console.log('#Comparing:', text);
for (let i = 0; i < pool.length; i++) {
    const similarity = cosSimilarity(text, pool[i], true);
    console.log('#TEXT:', pool[i], '#->', similarity);
}

// #Comparing: I want eat
// #TEXT: Drink me! #-> 0
// #TEXT: Eat me! #-> 0.33333333333333337
// #TEXT: I would like something to eat #-> 0.47140452079103173

```

## More info

@see https://en.wikipedia.org/wiki/Cosine_similarity